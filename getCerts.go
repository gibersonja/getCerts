package main

import (
	"crypto/dsa"
	"crypto/ecdh"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/tls"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

func main() {

	if len(os.Args) < 2 {
		er(fmt.Errorf("must specify at least one URL"))
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "-h" || arg == "--help" {
			exe, err := os.Executable()
			er(err)
			fmt.Printf("Usage: %s <URL|IP>:<PORT>\n", filepath.Base(exe))
			return
		}

		conf := &tls.Config{
			InsecureSkipVerify: true,
		}

		conn, err := tls.Dial("tcp", arg, conf)
		er(err)
		defer conn.Close()

		certs := conn.ConnectionState().PeerCertificates

		for _, cert := range certs {
			var keyusageS string
			var keyusage []string
			var mask uint16

			mask = 0b100000000
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Decipher Only")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Encipher Only")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "CRL Sign")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Key Certificate Sign")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Key Agreement")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Data Encipherment")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Key Encipherment")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Non-Repudiation")
			}

			mask = mask >> 1
			if uint16(cert.KeyUsage)&mask == mask {
				keyusage = append(keyusage, "Digital Signature")
			}

			for i := 0; i < len(keyusage); i++ {
				keyusageS = keyusageS + fmt.Sprintf("%s", keyusage[i])
				if i != len(keyusage)-1 {
					keyusageS = keyusageS + fmt.Sprint(",")
				}
			}

			fmt.Print("-----------------------------------------------------------------\n")
			fmt.Printf("Host:             %s\n", arg)
			fmt.Printf("Issuer:           %s\n", cert.Issuer)
			fmt.Printf("Subject:          %s\n", cert.Subject)
			fmt.Printf("Version:          %d\n", cert.Version)
			fmt.Printf("Serial Number:    %d\n", cert.SerialNumber)
			fmt.Printf("Signature Algo:   %s\n", cert.SignatureAlgorithm)
			fmt.Printf("Signature:        %X\n", cert.Signature)
			fmt.Printf("Public Key Algo:  %s\n", cert.PublicKeyAlgorithm)

			switch cert.PublicKey.(type) {
			case *rsa.PublicKey:
				fmt.Printf("Modulus:          %X\n", cert.PublicKey.(*rsa.PublicKey).N)
				fmt.Printf("Public Exponent:  %d\n", cert.PublicKey.(*rsa.PublicKey).E)
			case *ecdsa.PublicKey:
				fmt.Printf("Elliptic Curve:   %s\n", cert.PublicKey.(*ecdsa.PublicKey).Curve.Params().Name)
				fmt.Printf("Key Size:         %d\n", cert.PublicKey.(*ecdsa.PublicKey).Params().BitSize)
			case *dsa.PublicKey:
				fmt.Printf("P:                %v\n", cert.PublicKey.(*dsa.PublicKey).Parameters.P)
				fmt.Printf("Q:                %v\n", cert.PublicKey.(*dsa.PublicKey).Parameters.Q)
				fmt.Printf("G:                %v\n", cert.PublicKey.(*dsa.PublicKey).Parameters.G)
			case ed25519.PublicKey:
				fmt.Print("Public Key:       ")
				for j := 0; j < len(cert.PublicKey.(ed25519.PublicKey)); j++ {
					fmt.Printf("%X", cert.PublicKey.(ed25519.PublicKey)[j])
				}
				fmt.Print("\n")
			case *ecdh.PublicKey:
				fmt.Print("Public Key:       ")
				for k := 0; k < len(cert.PublicKey.(*ecdh.PublicKey).Bytes()); k++ {
					fmt.Printf("%X", cert.PublicKey.(*ecdh.PublicKey).Bytes()[k])
				}
				fmt.Print("\n")
			}

			fmt.Printf("Key Usage:        %s\n", keyusageS)
			fmt.Printf("Valid:            %s\n", cert.NotBefore.Format("2006.1.02 15:04:05"))
			fmt.Printf("Expire:           %s\n", cert.NotAfter.Format("2006.1.02 15:04:05"))
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
